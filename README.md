## Andersen Devops exam

This is CI/CD pipeline for "hello world" Go application.
Pipeline has 4 stages:
- test
- build
- scan
- deploy

![](/home/cube366/go_app/go_pipe.png)

### Test
Uses:
- golangci-lint
- go test (unit test)
- Sonarqube

### Build
Creating docker container and pushing to gitlab registry

### Scan
Scanning container with Trivy for vulnerabilities

### Deploy
Deploying container to AWS Elastic Kubernetes Services.

