module go_app

go 1.17

require (
	github.com/gin-gonic/gin v1.7.7
	github.com/stretchr/testify v1.4.0
)
