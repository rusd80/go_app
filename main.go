package main

import (
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
)

var router *gin.Engine

func index(c *gin.Context) {
	c.String(
		http.StatusOK,
		"Hello World 223456",
	)
}

func main() {
	gin.SetMode(gin.ReleaseMode)
	router = gin.Default()
	router.GET("/", index)
	routerErr := router.Run()
	if routerErr != nil {
		log.Fatal()
	}
}
