FROM golang:1.16-buster AS build

WORKDIR /app
COPY go.mod go.sum *.go ./
RUN go mod download

RUN CGO_ENABLED=0 GOOS=linux go build -a -ldflags '-extldflags "-static"' -o /binary


FROM alpine:3.15.0
WORKDIR /
COPY --from=build /binary /binary
EXPOSE 8080
ENTRYPOINT ["/binary"]
